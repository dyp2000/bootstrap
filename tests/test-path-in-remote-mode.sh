#!/usr/bin/env bash

TEST_PATH_IN_REMOTE_MODE_TEXT=""

function test_path_in_remote_mode() {
    TEST_PATH_IN_REMOTE_MODE_TEXT=$(cat "$(path "tests/test-path-in-remote-mode.txt")")
}
